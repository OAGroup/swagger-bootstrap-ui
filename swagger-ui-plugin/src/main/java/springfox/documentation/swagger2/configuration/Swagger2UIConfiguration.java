package springfox.documentation.swagger2.configuration;

import org.springframework.context.annotation.ComponentScan;

/**
 * @Copyright: Zhejiang Drore Technology Co., Ltd  2018 <br/>
 * @Desc: <br/>
 * @ProjectName: swagger-bootstrap-ui <br/>
 * @Date: 2018/9/23 20:43 <br/>
 * @Author: baoec@drore.com
 */
@ComponentScan(
        basePackages = {"springfox.documentation.swagger2.web"}
)
public class Swagger2UIConfiguration {
}
